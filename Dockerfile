# https://hub.docker.com/layers/golang/library/golang/1.14.11-alpine3.11/images/sha256-d8c613cb5ffaca3aadd093d0edf082db60898c697e6a2896ca111f2dea9ec170?context=explore
FROM golang:1.14.11-alpine3.11 AS build  
COPY simple_build/server.go /opt/src/
WORKDIR /opt/src
RUN go build server.go

# https://hub.docker.com/layers/alpine/library/alpine/3.11/images/sha256-b28e271d721b3f6377cb5bae6cd4506d2736e77ef6f70ed9b0c4716da8bdf17c?context=explore
FROM alpine:3.11
COPY --from=build /opt/src/server /opt/src/server
WORKDIR /opt/src
EXPOSE 8080
ENTRYPOINT [ "./server" ]